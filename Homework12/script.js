const buttons = document.querySelectorAll('.btn');


document.addEventListener('keydown', keyPress=>{
    const activeButton = document.querySelector('.active');
    if(activeButton){
        activeButton.classList.remove('active');
    }

    for (const button of buttons){
        if(keyPress.key.toUpperCase() === button.getAttribute('data-key-code') ){
            button.classList.add('active');
            break;
        }

    }
});



