const paragraph = document.querySelectorAll('p');

paragraph.forEach((e)=>{
    e.style.background = '#ff0000';
});

const optionsList = document.querySelector('#optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);

for (const child of optionsList.children) {
    console.log(`${child.nodeName} ${typeof child}`);
}

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';


const navItem = document.querySelector('.main-header');

for (const child of navItem.children){
    console.log(child);
    child.classList.add('nav-item');
}

const  sectionTitle = document.querySelectorAll('.section-title');

sectionTitle.forEach((e)=>{
    e.classList.remove('section-title');
})


