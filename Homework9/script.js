
const newUserArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

    const redEl = document.createElement('div');
    redEl.style.background = 'rgba(255, 0,0,0.3)';
    redEl.className = 'redDiv';
    document.body.appendChild(redEl);
    createListByArray(newUserArray);
    createListByArray(newUserArray, '.redDiv');


    function createListByArray(array, parentSelector) {
        const parentEl = document.querySelector(parentSelector) || document.body
        parentEl.appendChild(fillListByArray(array));
    }

    function fillListByArray(array) {
        const listEl = document.createElement('ul');
        array.forEach((element, idx) => {
            if (Array.isArray(element)) {
                listEl.appendChild(fillListByArray(element));
            } else {
                const listItem = document.createElement('li');
                listItem.innerText = array[idx];
                listEl.append(listItem);
            }
        });
        return listEl
    }

    function timerCountdown(from, to) {
    let current = to;
    let timerId = setInterval(function() {
        document.querySelector('.div_timer').innerText = `Документ буде очищенно через ${current}`;
        if (current == from) {
            clearInterval(timerId);
        }
        current--;
    }, 1000);
}
timerCountdown(0,3);
setTimeout(()=> { document.body.innerHTML=''}, 4000);