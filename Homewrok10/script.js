
const tabTitleEls = document.querySelectorAll('.tabs-title');
const tabTitleActiveClass = 'active'
//const tabTitleClass = 'tab-title'

openTab(document.querySelector(`.${tabTitleActiveClass}`))

tabTitleEls.forEach(titleEl => {
    titleEl.addEventListener('click', (event) => {
        const tabTitleEl = event.currentTarget;
        openTab(tabTitleEl)

    })
})

function openTab(titleEl) {
    const tabContentEl = document.querySelector('[data-tab-content="' + titleEl.dataset.tabTrigger + '"]');
    const currentActiveTab = document.querySelector(`.${tabTitleActiveClass}`)
    titleEl.classList.add(tabTitleActiveClass);
    if (currentActiveTab) {
        closeTab(currentActiveTab)
    }
    tabContentEl.style.display = 'block';
}

function closeTab(titleEl) {
    const previousTab = document.querySelectorAll('.tab-content');
    previousTab.forEach(closeTabs =>{
        closeTabs.style.display = '';
    })
    titleEl.classList.remove(tabTitleActiveClass);
}



