
let userName;
let userAge;

do{
    userName = prompt("Enter your name:", userName).trim();
    if( userName !== '' && userName.length >= 3){
        break;
    }
    alert("Not correct name, enter again!");
} while (true);

do{
    userAge = Number(prompt("Enter your age:", userAge));
    if(!isNaN(userAge)){
        break;
    }
} while (true);

switch (true){
    case (userAge <18):
        alert("You are not allowed to visit this website");
        break;
    case (userAge >=18 && userAge <=22):
        let access = confirm("Are you sure you want to continue?");
        if (access){
            alert(`Welcome ${userName}`);
        } else alert("You are not allowed to visit this website");
        break;
    case (userAge >22):
        alert(`Welcome ${userName}`);
        break;
    default:
        break;
}