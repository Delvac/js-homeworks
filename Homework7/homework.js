function filteredBy(arrayToFilter, typeToDelete){
    const newArray = arrayToFilter.filter(arr => typeof arr != typeToDelete);
    return newArray;
}

console.log(filteredBy(['hello', 'world', 23, '23', null], 'string'));
