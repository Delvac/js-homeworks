let newUser = new Object();

function createNewUser (){
    newUser = {
        fistName: "",
        lastName: "",
        birthday: null,
        getLogin: function (){
            return (this.fistName[0]+this.lastName).toLowerCase();
        },
        setFirstName: function (){
            Object.defineProperty(this, "firstName",{
                writable: true,
            })
            return this.fistName = prompt("Enter your first name:")
        },
        setLastName: function (){
            Object.defineProperty(this, "lastName",{
                writable: true,
            })
            return this.lastName = prompt("Enter your last name:")
        },
        setUserAge: function (){
            return this.birthday = prompt("Enter your age in format dd.mm.yyyy :");
        },
        getAge: function (){
            const userAge = new Date(this.birthday.split('.')[2], this.birthday.split('.')[1] - 1, this.birthday.split('.')[0]);
            const yearDifference = new Date(Date.now() - userAge.getTime()).getUTCFullYear();
            const age = Math.abs(yearDifference - 1970);
            return `Your age is ${age}`;
        },
        getPassword: function (){
            return this.fistName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthday.split('.')[2];
        }
    }
    return newUser;
}
Object.defineProperties(newUser, {
    firstName:  {
        writable: false
    },
    lastName: {
        writable:false
    }
})




createNewUser();
newUser.setFirstName();
newUser.setLastName();
newUser.setUserAge();

console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log(createNewUser());
//console.log(newUser.getLogin());
